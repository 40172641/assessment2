﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Author Kevin Falconer 40172641
//Class for Dishes Class
//Date Last Modifieid 11/12/2015
namespace cw2
{
   public class Dishes
    {
        //Fields 
        private string description;
        private Boolean vegiterian;
        private int price;



        //Get and Set methods for Description of type string 
        public string Description
        {
            get { return description; } //Get Method
            set { description = value; } //Set Method
        }
        //Get and Set methods for Vegiterian of type Boolean
        //Also determines whether the User is a vegiterian or not
        public Boolean Vegiterian
        {
            get { return vegiterian; }
            set
            {
                if (value == true) //Example code
                { 
                    vegiterian = value;
                }
                else
                {
                    vegiterian = false;
                }
            } // end of Vegiterian Method
        }
        //Get and Set methods for Price attribute
        //Validation so that values between 0 and 100000 pence are accepted and any value greater thorws and exceptiom
        public int Price
        {
            get { return price; }
            set
            {
                if(price >= 0 && price <= 10000) //Validation
                {
                    price = value;
                }
                else
                {
                    throw new Exception("Price between 0 and 10000 pence only!"); //Throws exception 
                }
            }
        }
        public override string ToString()
        {
            return (Description + " " + "£" + Convert.ToInt32(Price) + " " +Convert.ToBoolean(Vegiterian));
        }

        public string unitTest()
        {
            Dishes unitTestDish = new Dishes();
            unitTestDish.Description = "Cheese & Tomato Pizza";
            unitTestDish.Price = 2;
            unitTestDish.Vegiterian = true;
            return unitTestDish.Description + unitTestDish.Price + unitTestDish.Vegiterian;
        }
        // End of method
    } // end of class
}       