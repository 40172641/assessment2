﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Author Kevin Falconer 40172641
//Class for Manager Class
//Date Last Modifieid 11/12/2015
namespace cw2
{
    public partial class Manager : Form
    {
        public Manager()
        {
            InitializeComponent();
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }
        List<Dishes> takeList; // Empty List
        List<Server> takeListServer; //Empty List
        List<Driver> takeListDriver; // Empty List

        public void takeListItem(List<Dishes> t)
        {
            takeList = t;
        }

        public void takeListServerMethod(List<Server> s)
        {
            takeListServer = s;
        }


        public void takeListDriverMethod(List<Driver> d)
        {
            takeListDriver = d;
        }
        //Adds items to the Dishes Listbox
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Dishes addNewItem = new Dishes(); //Menu Object 
            //Try Catch is used for input validation, and will throw exceptions
            try
            {
                addNewItem.Price = Convert.ToInt32(textBoxMenuPrice.Text); //Inputs the Items Price and Updates Menu Class
                addNewItem.Description = textBoxDesc.Text; //Inputs the items prices and Updates the Menu Class
                
                if (checkBox1.Checked)
                {
                    addNewItem.Vegiterian = true;
                }
                else
                {
                    addNewItem.Vegiterian = false;
                }
                takeList.Add(addNewItem); //Adds to Dishes Listbox
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }
            catch (Exception item)
            {
                MessageBox.Show(item.Message); // Outputs Error Exception
            }

            if (textBoxDesc == null) 
            {
                MessageBox.Show("Cannot be a Null Value");
                takeList.Remove(addNewItem); //Removes item if null
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }
            if (textBoxMenuPrice == null)
            {
                takeList.Remove(addNewItem); //Removes item if null
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }

        }
        //Adds items to the Server Listbox
        private void buttonAddServer_Click(object sender, EventArgs e)
        {
            Server addServer = new Server(); //Server Object

            //Try Catch is used for input validation, and will throw exceptions
            try
            {
                addServer.ServerName = textBoxServerName.Text; //Inputs Server Name and Adds to Updates Server Class
                addServer.ServerID = Convert.ToInt32(textBoxServerID.Text); //Inputs Server ID and updates Server Class

                takeListServer.Add(addServer); //Adds to Server Listbox
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
            catch (Exception server)
            {
                MessageBox.Show(server.Message); //Outputs Exception Error
            }
            
            if (addServer.ServerName == null)
            {
                takeListServer.Remove(addServer); //Adds to Driver Listbox
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
            if (textBoxServerID == null)
            {
                takeListServer.Remove(addServer); //Adds to Driver Listbox
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
        }
        //Adds items to the driver listbox
        private void buttonAddDriver_Click(object sender, EventArgs e)
        {

            Driver addDriver = new Driver(); //Driver Object
            //Try Catch is used for input validation, and will throw exceptions
            try
            {
                addDriver.DriverName = textBoxDriver.Text; //Inputs Driver Name and Updates driver class
                addDriver.DriverID = Convert.ToInt32(textBoxDriverID.Text); //Inputs DriverID and Updates Driver Class
                addDriver.DriverRegistration = textBoxCarReg.Text; //Inputs Driver Registration Number and Updates Driver Class

                takeListDriver.Add(addDriver); //Adds to Driver Listbox
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }
            catch (Exception driverException)
            {
                MessageBox.Show(driverException.Message); //Outputs exception Error
            }
             if (textBoxDriverID == null)
            {
                takeListDriver.Remove(addDriver); //Adds to Driver Listbox
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }

            if (addDriver.DriverName == null)
            {
                takeListDriver.Remove(addDriver); //Adds to Driver Listbox
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }
            if(textBoxCarReg == null)
            {
                takeListDriver.Remove(addDriver); //Adds to Driver Listbox
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }

    
        }

        private void Remove_Click(object sender, EventArgs e)
        {
            if (listBoxDishes.SelectedItem != null)
            {
                takeList.RemoveAt(listBoxDishes.SelectedIndex); //Removes Selected Item
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }



        }

        private void buttonRemoveServer_Click(object sender, EventArgs e)
        {
            if (listboxManagerServer.SelectedItem != null)
            {
                takeListServer.RemoveAt(listboxManagerServer.SelectedIndex); //Remvoes Selected Item
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
        }


        private void buttonDriverDriver_Click(object sender, EventArgs e)
        {

            if (listBoxDriver.SelectedItem != null)
            {
                takeListDriver.RemoveAt(listBoxDriver.SelectedIndex); //Removes Selected Item
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }

        }

        private void buttonAmend_Click(object sender, EventArgs e)
        {
            Dishes addNewItem = new Dishes(); //Menu Object 
            //Try Catch is used for input validation, and will throw exceptions
            try
            {
                addNewItem.Price = Convert.ToInt32(textBoxMenuPrice.Text); //Inputs the Items Price and Updates Menu Class
                addNewItem.Description = textBoxDesc.Text; //Inputs the items prices and Updates the Menu Class

                if (checkBox1.Checked)
                {
                    addNewItem.Vegiterian = true;
                }
                else
                {
                    addNewItem.Vegiterian = false;
                }
                takeList.RemoveAt(listBoxDishes.SelectedIndex); //Removes Selected Item
                takeList.Add(addNewItem); //Adds Selected Item
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }
            catch (Exception item)
            {
                MessageBox.Show(item.Message); // Outputs Error Exception
            }

            if (addNewItem.Description == null)
            {
                takeList.Remove(addNewItem); //Removes item if null
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }
            if (textBoxMenuPrice == null)
            {
                takeList.Remove(addNewItem); //Removes item if null
                listBoxDishes.DataSource = null;
                listBoxDishes.DataSource = takeList;
            }


        }

        private void buttonAmendServer_Click(object sender, EventArgs e)
        {
            Server addServer = new Server(); //Server Object

            //Try Catch is used for input validation, and will throw exceptions
            try
            {
                addServer.ServerName = textBoxServerName.Text; //Inputs Server Name and Adds to Updates Server Class
                addServer.ServerID = Convert.ToInt32(textBoxServerID.Text); //Inputs Server ID and updates Server Class
                takeListServer.RemoveAt(listboxManagerServer.SelectedIndex); //Removes selected item
                takeListServer.Add(addServer); //Adds new item to listbox
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
            catch (Exception server)
            {
                MessageBox.Show(server.Message); //Outputs Exception Error
            }
            ;

            if (addServer.ServerName == null)
            {
                takeListServer.Remove(addServer); //Removes Item if  null
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
            if (textBoxServerID == null)
            {
                takeListServer.Remove(addServer); //Removes Item if  null
                listboxManagerServer.DataSource = null;
                listboxManagerServer.DataSource = takeListServer;
            }
        }
        

        private void buttonAmendDriver_Click(object sender, EventArgs e)
        {
            Driver addDriver = new Driver(); //Driver Object
            //Try Catch is used for input validation, and will throw exceptions
            try
            {
                addDriver.DriverName = textBoxDriver.Text; //Inputs Driver Name and Updates driver class
                addDriver.DriverID = Convert.ToInt32(textBoxDriverID.Text); //Inputs DriverID and Updates Driver Class
                addDriver.DriverRegistration = textBoxCarReg.Text; //Inputs Driver Registration Number and Updates Driver Class
                takeListDriver.RemoveAt(listBoxDriver.SelectedIndex); //Removes Selected Item
                takeListDriver.Add(addDriver); //Adds Item to the Listbox
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }
            catch (Exception driverException)
            {
                MessageBox.Show(driverException.Message); //Outputs exception Error
            }

            if (textBoxDriverID == null)
            {
                takeListDriver.Remove(addDriver); //Removes Item if  null
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }

            if (addDriver.DriverName == null)
            {
                takeListDriver.Remove(addDriver); //Removes Item if  null
                listBoxDriver.DataSource = null;
                listBoxDriver.DataSource = takeListDriver;
            }

        }

        }
    }
 
