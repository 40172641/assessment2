﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    class DataStore
    {
        private List<Delivery> deliveryOrder = new List<Delivery>(); //List for Delivery Orders
        private List<Sit_In_Orders> sitInOrder = new List<Sit_In_Orders>(); //List for SitInOrders
        
        public List<Delivery> listOfDelivery
        {
            get { return deliveryOrder; } //Get Method for Delivery Order
        }
        public List<Sit_In_Orders> listOfSitIns
        {
            get { return sitInOrder; } //Get method for SitIn Order
        }
        public void addNewDeliveryOrder(Delivery order)
        {
            deliveryOrder.Add(order); //Adds Delivery Order
        }

        public void addNewSitInOrder(Sit_In_Orders order)
        {
            sitInOrder.Add(order); //Adds SitIn Order
        }
    }
}
