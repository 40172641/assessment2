﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//Author Kevin Falconer 40172641
//Class for MainWindow
//Date Last Modifieid 11/12/2015
namespace cw2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Used to Hide Parts of the GUI when a radio button is clicked
        private void radioButtonSitInOrder_Checked(object sender, RoutedEventArgs e)
        {
            textBoxTableNo.Visibility = Visibility.Visible;//Shows Textbox
            comboBoxServer.Visibility = Visibility.Visible;//Shows Combobox
            textBoxSitInName.Visibility = Visibility.Visible;//Shows textbox
            labelSitInNmae.Visibility = Visibility.Visible;//Shows Label
            labelTable.Visibility = Visibility.Visible;//Shows Label
            labelServer.Visibility = Visibility.Visible;//Shows Label
            labelPickServer.Visibility = Visibility.Visible;//Shows Label
            textBoxDeliveryName.Visibility = Visibility.Hidden;//Hides Textbox
            textBoxDeliveryAddress.Visibility = Visibility.Hidden; //Hides Textbox
            listBoxDrivers.Visibility = Visibility.Hidden; //Hides Listbox
            labelDeliveryName.Visibility = Visibility.Hidden; //Hides Label
            labelDeliveryAddress.Visibility = Visibility.Hidden; //Hides Label
            labelDrivers.Visibility = Visibility.Hidden;//Hides Label
            labelSelectDriver.Visibility = Visibility.Hidden; //Hides Label


        }
        private void radioButtonDelivery_Checked(object sender, RoutedEventArgs e)
        {
            textBoxTableNo.Visibility = Visibility.Hidden;//Hides textbox
            comboBoxServer.Visibility = Visibility.Hidden;//Hides combobox
            textBoxSitInName.Visibility = Visibility.Hidden; //Hides textbox
            labelSitInNmae.Visibility = Visibility.Hidden; //Hides Label
            labelTable.Visibility = Visibility.Hidden;//Hides Label
            labelServer.Visibility = Visibility.Visible;//Hides Label
            labelPickServer.Visibility = Visibility.Visible;//Hides Label
            textBoxDeliveryName.Visibility = Visibility.Visible; //Shows Textbox
            textBoxDeliveryAddress.Visibility = Visibility.Visible; //Shows Textbox
            comboBoxServer.Visibility = Visibility.Visible; //Hides ComboBox
            listBoxDrivers.Visibility = Visibility.Visible;//Shows Label
            labelSelectDriver.Visibility = Visibility.Visible;//Shows Label
            labelDrivers.Visibility = Visibility.Visible;//Shows Label
            labelDeliveryName.Visibility = Visibility.Visible; //Shows Label
            labelDeliveryAddress.Visibility = Visibility.Visible; //Shows Label
           
        }


        Delivery delivery = new Delivery(); //New Object for Name
        Sit_In_Orders sitIn = new Sit_In_Orders(); //New Object for Table
        Dishes newOrder = new Dishes();//Object for the Dishes Used for Creating a new Order
        DataStore newDataStorage = new DataStore(); //Object for the Datastore Class

        List<Dishes> newListItem = new List<Dishes>(); //New List Item for Dishes
        List<Server> newListServer = new List<Server>(); //New List Item for Server
        List<Driver> newListDriver = new List<Driver>();// New List Item for Driver


        public void buttonOrder_Click(object sender, RoutedEventArgs e)
        {            
            if (radioButtonSitInOrder.IsChecked == true) //If the SitIn Order RadioButton is checked
            {
                //Try Catch is used for input validation, and will throw exceptions
                try
                {
                    delivery.CustomerName = textBoxSitInName.Text; //Updates the Class with Customer Name
                    sitIn.Table = Convert.ToInt32(textBoxTableNo.Text);  //Updates Class with Table Number
                    sitIn.Server = comboBoxServer.SelectedItem.ToString(); //Server is String of Selected Item
                    sitIn.Dish = listBoxDishes.SelectedItem.ToString(); //Dishes is string of selected item
                    newDataStorage.addNewSitInOrder(sitIn); //Adds Item to List
                    sitIn = new Sit_In_Orders(); //Stops the List from Overwritting each time a new order is added
                    foreach (Sit_In_Orders s in newDataStorage.listOfSitIns)
                    {
                        MessageBox.Show("Order Added");
                        MessageBox.Show("Customer Name:" + " " + s.Name + "\r\n" + "Table No: " + s.Table + "\r\n" + "Server ID: " + s.Server + "\r\n" + "Item: " + s.Dish + "\r\n");
                    }
                }
                catch (Exception sitIn)
                {
                    MessageBox.Show(sitIn.Message); //Outputs Exception Error 
                }
                textBoxSitInName.Clear(); //Clears textbox
                textBoxTableNo.Clear(); //Clears Textbox
            }
            if (radioButtonDelivery.IsChecked == true) //If the RadioButton Delivery is checked
            {
                //Try Catch is used for input validation, and will throw exceptions
                try
                {
                    delivery.CustomerName = textBoxDeliveryName.Text; //Updates Class with Customer Name
                    delivery.DeliveryAddress = textBoxDeliveryAddress.Text; //Updates Class with Customer Address
                    delivery.Server = comboBoxServer.SelectedItem.ToString(); //Converts Selected Item to String Value
                    delivery.DeliveryDriver = listBoxDrivers.SelectedItem.ToString(); //Converts Selected Item to String Value
                    delivery.Dishes = listBoxDishes.SelectedItem.ToString(); //Converts Selected Item to String Value
                    newDataStorage.addNewDeliveryOrder(delivery);  //Adds a New Order 
                    delivery = new Delivery(); //Stops the List from Overwritting each time a new order is added
                    foreach (Delivery d in newDataStorage.listOfDelivery)
                    {  
                        MessageBox.Show("Order Added");
                        MessageBox.Show("Customer Name:" + " " + d.CustomerName + "\r\n" + "Delivery Address: " + d.DeliveryAddress + "\r\n" + "Server ID: " + d.Server + "\r\n" + "Item: " + d.Dishes + "\r\n" + "Driver ID: " + d.DeliveryDriver);
                    }
                    textBoxDeliveryName.Clear(); //Clears the texbox 
                    textBoxDeliveryAddress.Clear(); //Clears the Textbox
   
                }
                catch (Exception deliveryExcept)
                {
                    MessageBox.Show(deliveryExcept.Message); //Outputs Exception Error 
                }
            }
        }//End of Add New Order Button
        
    

    private void buttonManager_Click(object sender, RoutedEventArgs e)
        {
            //Hides Manager Window
            Manager newManager = new Manager(); //Adds new manager object
            newManager.takeListItem(newListItem);//Accesses Method in Manager, used to get listbox values from other form
            newManager.takeListServerMethod(newListServer);//Accesses Method in Manager, used to get listbox values from other form
            newManager.takeListDriverMethod(newListDriver); //Accesses Method in Manager, used to get listbox values from other form
            newManager.ShowDialog(); //Show's Manager Window
            listBoxDishes.ItemsSource = null;
            listBoxDishes.ItemsSource = newListItem;//Listbox equals the values in the List
            comboBoxServer.ItemsSource = null;
            comboBoxServer.ItemsSource = newListServer;//Listbox equals the values in the List
            listBoxDrivers.ItemsSource = null;
            listBoxDrivers.ItemsSource = newListDriver; //Listbox equals the values in the List       
        }
        
        private void btnCreateBill_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonDelivery.IsChecked == true)
            {
                delivery = new Delivery();
                foreach (Delivery d in newDataStorage.listOfDelivery)
                {
                    textBoxCreateBill.Text = "Customer Name " + d.CustomerName + "\r\n" + "Delivery Address: " + d.DeliveryAddress + "\r\n" + "ServerID: " + d.Server + "\r\n" + "Delivery Driver: " + d.DeliveryDriver + "\r\n" + "Item: " + d.Dishes + "\r\n" ;
                    MessageBox.Show("Customer Name:" + " " + d.CustomerName + "\r\n" + "Delivery Address: " + d.DeliveryAddress + "\r\n" + "Server ID: " + d.Server + "\r\n" + "Item: " + d.Dishes + "\r\n" + "Driver ID: " + d.DeliveryDriver);
                }
            }
            if (radioButtonSitInOrder.IsChecked == true)
            {
               
                sitIn = new Sit_In_Orders();
                foreach (Sit_In_Orders s in newDataStorage.listOfSitIns)
                {
                    MessageBox.Show("Customer Name:" + " " + delivery.CustomerName + "\r\n" + "Table No: " + s.Table + "\r\n" + "Server ID: " + s.Server + "\r\n" + "Item: " + s.Dish + "\r\n");
                    textBoxCreateBill.Text = "Customer Name:" + " " + delivery.CustomerName + "\r\n" + "Table No: " + s.Table + "\r\n" + "Server ID: " + s.Server + "\r\n" + "Item: " + s.Dish + "\r\n";
                }
            }
        }
    }
}
