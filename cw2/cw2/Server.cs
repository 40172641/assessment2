﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
   public class Server
    {
        //Fields
        private string serverName;
        private int serverID;
        
        //Get and Set methods for the name of the server of type string 
        public string ServerName
        {
            get { return serverName; } //Get Method
            set {serverName = value;} //Set Method
        }
        
        
        //Get and Set methods for the ID Number of the Server
        public int ServerID
        {
            get { return serverID; }
            set
            {
                if(serverID >= 0 && serverID <=100000)//Validation for ID Number
                {
                    serverID = value;
                }
                else
                {
                    throw new Exception("Not within ID Range"); //Throws new exception if not in range
                }
            }
        }
        public override string ToString()
        {
            return (ServerName + " " + "ID: " + Convert.ToString(ServerID));
        }

        public string unitTestServer()
        {
            Server unitTestServer = new Server();
            unitTestServer.ServerName = "Kevin";
            unitTestServer.ServerID = 3;
            return unitTestServer.ServerName + unitTestServer.ServerID;
        }

    }
}
