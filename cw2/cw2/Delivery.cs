﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//Author Kevin Falconer 40172641
//Class for DataStore Class
//Date Last Modifieid 11/12/2015
namespace cw2 
{
    public class Delivery
    {
        //fields 
        private string customerName;
        private string deliveryAddress;
        List<Dishes> items = new List<Dishes>();
        private string server; //Used for the Selected Item String in Main Window
        private string deliveryDriver; //Used for the Selected Item String in Main Window
        private string dishes; //Used for the Selected Item String in Main Window


        public void addOrder(Dishes newOrder)
        {
            items.Add(newOrder); //Adds a New Customer Order
        }

        //Get and Set Methods for Customer Name which is type String
        public string CustomerName
        {
            get { return customerName; } //Returns Customer Name
            set  { customerName = value;} //Sets Customer Name
        }

        //Get and Set Methods for Delivery Address which is type String
        public string DeliveryAddress
        {
            get { return deliveryAddress; } //Gets delivery address
            set  {  deliveryAddress = value; }//Sets delivery address
        }

        public string Server
        {
            get { return server; }
            set { server = value; }
        }

        public string DeliveryDriver
        {
            get { return deliveryDriver; }
            set { deliveryDriver = value; }
        }

        public string Dishes
        {
            get { return dishes;}
            set { dishes = value; }
        }
        public void ShowOrder()
        {
            foreach (Dishes d in items)
            {
                MessageBox.Show("Name:" + " " + CustomerName + "Address: " + " " + DeliveryAddress);               
            }
        }

    }    

    public class Driver : Delivery
    {
        //Fields
        private string driverName;
        private int driverID;
        private string driverRegistration;

        //Set and Get method for the Driver Name
        public string DriverName
        {
            get { return driverName; } //Get Method
            set { driverName = value; } //Set Method
        }//End of DriverName

        //Set and Get methods for the Driver ID
        public int DriverID
        {
            get { return driverID; } //Get Method
            set
            {
                if (driverID >= 0 && driverID <= 50000) //Validation for DriverID, has to be between 0 & 50000
                {
                    driverID = value;
                }
                else
                {
                    throw new Exception("Not within ID range!!!"); //Throws exception error
                }
            }//End of loop
        }//End of DriverID

        //Get and Set methods for Driver Registration
        public string DriverRegistration
        {
            get { return driverRegistration; } //Get Method
            set { driverRegistration = value; } //Set Method
        }

        public override string ToString()
        {
            return (DriverName + " " + "ID: " + Convert.ToString(DriverID) + " " + DriverRegistration);
        }
    }


}//End of Namespace

