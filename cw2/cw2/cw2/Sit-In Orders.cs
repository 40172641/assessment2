﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    class Sit_In_Orders
    {
        //fields 
        private int table;
        //items 

        //Get and Set Method for table of type integer
        //Validation so that values between 1 and 10 are accepted
        public int Table
        {
            get { return table; }
            set
            {
                if (table >= 1 && table <= 10)
                {
                    table = value;
                }
                else
                {
                    throw new Exception("Not within table range");
                }
            }
            //Item Attribute
        }
    }
}

    
