﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    class Menu
    {
        //Fields 
        private string description;
        private Boolean vegiterian;
        private int price;



        //Get and Set methods for Description of type string 
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        //Get and Set methods for Vegiterian of type Boolean
        //Also determines whether the User is a vegiterian or not
        public Boolean Vegiterian
        {
            get { return vegiterian; }
            set
            {
                if (vegiterian == true) //Example code
                {
                    vegiterian = true;
                }
                else
                {
                    vegiterian = false;
                }
            } // end of Vegiterian Method

            //Get and Set methods for Price attribute
            //Validation so that values between 0 and 100000 pence are accepted and any value greater thorws and exceptiom
           public int Price
        {
            get { return price; } //returns price
            set
            {
                if (price >= 0 && price <= 100000)
                {
                    price = value;
                }
                else
                {
                    throw new Exception("Please re-enter");
                }
            }
        }        
     }
} //end of Price Method
//end of class
