﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//Author Kevin Falconer 40172641
//Class for SitInOrder and Server Classes
//Date Last Modifieid 11/12/2015
namespace cw2
{
   public class Sit_In_Orders
    {
        //fields 
        private int table;
        List<Dishes> items = new List<Dishes>();
        private string server; // Used for the Selected Item in Main Window
        private string dish; // Used for the Selected Item in Main Window
        private string name; // Used for Customer Name in Sit In Order

        public void addOrder(Dishes newOrder)
        {
            items.Add(newOrder); //Adds a New Customer Order
        }
        //Get and Set Method for table of type integer
        //Validation so that values between 1 and 10 are accepted
        public int Table
        {
            get { return table; }
            set
            {
                if (table <= 1 && table >= 10) //Validation
                {
                    throw new Exception("Not within table range");//Exception Testing
                }
                table = value;
            }
        }
        //Item Attribute
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        //Get and Set Method for String Dish, dish is used to output the selected index of the Server ComboBox in a Sit In Order
        public string Server
        {
            get { return server; }
            set { server = value; }
        }
        //Get and Set Method for String Dish, dish is used to output the selected index of the Dishes Listbox in a Sit In Order
        public string Dish
        {
            get { return dish; }
            set { dish = value;}
        }

        public void addItem(Dishes newOrder)
        {
            items.Add(newOrder);
        }


        //foreach loop for Items List
        public void addOrderSitIn()
        {
            foreach (Dishes d in items)
            {
                MessageBox.Show("Sit In Order Added"); //Outputs that Sit In Order has been added
            }

        }


    } //End of Class

    public class Server : Sit_In_Orders
    {
        //Fields
        private string serverName;
        private int serverID;

        //Get and Set methods for the name of the server of type string 
        public string ServerName
        {
            get { return serverName; } //Get Method
            set { serverName = value; } //Set Method
        }


        //Get and Set methods for the ID Number of the Server
        public int ServerID
        {
            get { return serverID; }
            set
            {
                if (serverID >= 0 && serverID <= 100000)//Validation for ID Number
                {
                    serverID = value;
                }
                else
                {
                    throw new Exception("Not within ID Range"); //Throws new exception if not in range
                }
            }
        }
        public override string ToString()
        {
            return (ServerName + " " + "ID: " + Convert.ToString(ServerID));
        }

        //Unit Testing
        public string unitTestServer()
        {
            Server unitTestServer = new Server(); //New Server Object
            unitTestServer.ServerName = "Kevin"; //Server Name
            unitTestServer.ServerID = 3; //ServerID
            return unitTestServer.ServerName + unitTestServer.ServerID;
        }
    }
} //End of Namespace CW2

    
