﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw2
{
    public class Driver
    {
        //Fields
        private string driverName;
        private int driverID;
        private string driverRegistration;

        //Set and Get method for the Driver Name
        public string DriverName
        {
            get { return driverName; } //Get Method
            set { driverName = value; } //Set Method
        }//End of DriverName

        //Set and Get methods for the Driver ID
        public int DriverID
        {
            get { return driverID; } //Get Method
            set
            {
                if (driverID >= 0 && driverID <= 50000) //Validation for DriverID, has to be between 0 & 50000
                {
                    driverID = value;
                }
                else
                {
                    throw new Exception("Not within ID range!!!"); //Throws exception error
                }
            }//End of loop
        }//End of DriverID

        //Get and Set methods for Driver Registration
        public string DriverRegistration
        {
            get { return driverRegistration; } //Get Method
            set { driverRegistration = value; } //Set Method
        }

        public override string ToString()
        {
            return (DriverName + " " + "ID: " + Convert.ToString(DriverID) + " " + DriverRegistration);
        }
    }
}