﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cw2;
//Author Kevin Falconer 40172641
//Class for Unit Testing
//Date Last Modifieid 11/12/2015
namespace assessment2UnitTests
{
    [TestClass]
    public class ServerUnitTest
    {
        [TestMethod]
        public void testMethodCheckServer()
        {
            Server unitTest = new Server(); //
            string expected = "Kevin" + 3; // expected result of the the unit test
            string actual; //actual result of the unit test 
            actual = unitTest.unitTestServer(); // actual is equal to the method in Server

            Assert.AreEqual(expected, actual);



        }
        [TestMethod]
        public void testMethodCheckDishes()
        {
            Dishes unitTest = new Dishes(); //New Dishes Object for Unit Testing
            string expected = "Cheese & Tomato Pizza" + 2 + true; //Exepected result of the Unit Test
            string actual; //Actual results of the unit test
            actual = unitTest.unitTest(); //Actual is equal to the Method made in Dishes.

            Assert.AreEqual(expected, actual);
        }
    }
}
